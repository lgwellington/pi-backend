# base-back

# Executing in your environment

1. Maven update project
2. Maven clean/install
3. Configurar servidor para executar local (Utilizado Tomcat v9.0)
4. Baixar/ executar o MySQL (configuração padrão)
5. Criar conexão MySQL (Connection Name: user / username: root / password: root)
6. Se necessário, baixar e configurar driver MySQL (Build Path/ Add external Jar / .jar do MySQL)
7. Ainda nessa configuração, adicionar no Deployment Assembly esse drive;
8. Testar conexão com DB via projeto (ConnectionUtils.java)
9. Utilizar scriptDB em "src/main/resources" para criar DB
10. Executar o projeto