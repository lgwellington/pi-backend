CREATE DATABASE pi;

USE pi;
DROP DATABASE pi;

CREATE TABLE cliente (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nomeCompleto VARCHAR(50) NOT NULL,
	cpf VARCHAR(11) NOT NULL UNIQUE,
    dataNasc VARCHAR(10) NOT NULL,
    email VARCHAR(50) NOT NULL,
    senha VARCHAR(20) NOT NULL,
    sexo VARCHAR(10) NOT NULL,
    endereco VARCHAR(50) NOT NULL, 
    complemento VARCHAR(10),
    formaPagamento VARCHAR(20) NOT NULL
);

CREATE TABLE produto (
	id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
	nome VARCHAR(50) NOT NULL UNIQUE,
	tamanho VARCHAR(10) NOT NULL,
    preco DOUBLE NOT NULL,
    qtdEstoque INTEGER NOT NULL,
    categoria VARCHAR(10) NOT NULL
);

CREATE TABLE venda (
  id INTEGER NOT NULL AUTO_INCREMENT,
  idCliente INTEGER NOT NULL,
  idProduto INTEGER NOT NULL,
  qtdProduto INTEGER NOT NULL,
  totalVenda DOUBLE NOT NULL,
  dataVenda VARCHAR(10) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT id_Cliente
    FOREIGN KEY (idCliente)
    REFERENCES cliente(id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT id_Produto
    FOREIGN KEY (idProduto)
    REFERENCES produto (id)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION
);
    
SELECT * FROM cliente;
SELECT * FROM venda;
SELECT * FROM produto;

SELECT v.id, c.nomeCompleto, c.cpf, p.nome, p.preco, p.qtdEstoque, v.totalVenda, v.dataVenda
				FROM venda AS v JOIN cliente AS c ON v.idCliente = c.id JOIN produto AS p ON v.idProduto = p.id
