package br.senac.backend.servicos;

import java.util.Calendar;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import br.senac.backend.db.dao.DaoVenda;
import br.senac.backend.model.Venda;

@Path("/venda")
public class ServicoVenda {

	Calendar cal = Calendar.getInstance();
	int year = cal.get(Calendar.YEAR);

	@POST
	@Consumes(MediaType.APPLICATION_JSON)
	//@Produces(MediaType.APPLICATION_JSON)
	public Response inserirVenda(Venda venda) {

		Response response = null;

		try {

			if (!venda.getDataVenda().isEmpty() && !venda.getTotalVenda().isNaN()) {

				if (venda.getDataVenda().length() > 10) {
					response = Response.status(Response.Status.OK.getStatusCode())
							.entity("O n�mero m�ximo de caracteres para data � 10").build();
				} 		
				
				else {

						DaoVenda.inserir(venda);
						response = Response.status(Response.Status.OK.getStatusCode()).entity("SUCESSO").build();
					
					return response;
				}
			} else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode())
						.entity("Preencha todos os campos para prosseguir...").build();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		return response;
	}

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Venda> listarVendas() {

		try {
			return DaoVenda.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public Response atualizarVenda(Venda venda) {

		Response response = null;

		try {

			if (!venda.getDataVenda().isEmpty() && !venda.getTotalVenda().isNaN()) {

				if (venda.getDataVenda().length() > 10) {
					response = Response.status(Response.Status.OK.getStatusCode())
							.entity("O n�mero m�ximo de caracteres para data � 10").build();
				} else {

						DaoVenda.atualizar(venda);
						response = Response.status(Response.Status.OK.getStatusCode()).entity("SUCESSO").build();
					
					return response;
				}
			} else {
				response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode())
						.entity("Preencha todos os campos para prosseguir.").build();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return response;
	}

	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/{id}")
	public Response removerVenda(@PathParam("id") Integer id) {

		Response response = null;

		try {
			DaoVenda.excluir(id);
			response = Response.status(Response.Status.OK.getStatusCode()).entity("Deletado com sucesso").build();
		} catch (Exception e) {
			response = Response.status(Response.Status.EXPECTATION_FAILED.getStatusCode())
					.entity("Venda n�o encontrada").build();
			e.printStackTrace();
		}

		return response;

	}
}
